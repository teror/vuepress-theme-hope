import { Logger } from "@vuepress/helper";

import { PLUGIN_NAME } from "../constant.js";

export const logger = new Logger(PLUGIN_NAME);
